class Graph:
    def __init__(self):
        self._nodes = {}
        self._edges = {}
        self._edgeslist =[]
    @property
    def node(self):
        return self._nodes
    
    @property
    def edge(self):
        return self._edges
    
    def nodes(self):
        return list(self._nodes.keys())
    
    def edges(self):
        return self._edgeslist
    
    def add_node(self, node, attr_dict={}):
        self._nodes[node]=attr_dict
    
    def add_edge(self, node1, node2, attr_dict={}):
        if not (node1 in self._nodes):
            self.add_node(node1)
        if not (node2 in self._nodes):
            self.add_node(node2)
        if (node1 in self._edges):
            self._edges[node1].update({node2:attr_dict})
        else:
            self._edges[node1] = {node2:attr_dict}
        
        if (node2 in self._edges):
            self._edges[node2].update({node1:attr_dict})
        else:
            self._edges[node2] = {node1:attr_dict}
        # self._edges.setdefault(node1,{})[node2] = attr_dict
        # self._edges.setdefault(node2,{})[node1] = attr_dict
            
        self._edgeslist.append((node1,node2))
            
    def add_nodes_from(self, node_list, attr_dict={}):
        for i in node_list:
            self.add_node(i,attr_dict)
    
    def add_edges_from(self, edge_list, attr_dict={}):
        for i in edge_list:
            self.add_edge(i[0],i[1],attr_dict)
    
    def degree(self,node):
        return len(self._edges[node])
    
    def __getitem__(self, node):
        return self._edges[node]
    
    def __len__(self):
        return len(self._nodes)
    
    def neighbors(self, node):
        return list(self._edges[node].keys())
    
    def remove_node(self, node1):
        self._nodes.pop(node1)
        llista = list(self._edges[node1].keys())
        for key in llista:
             self.remove_edge(node1,key)
        self._edges.pop(node1)

    def remove_edge(self, node1, node2):
        try:
            self._edges[node1].pop(node2)
            self._edges[node2].pop(node1)
            try:
                self._edgeslist.remove((node1,node2))
            except:
                self._edgeslist.remove((node2,node1))
        except:
            print("Edge (",node1,",",node2,") doesn't exists", sep="")
    def remove_nodes_from(self, node_list):
        for node in node_list:
            self.remove_node(node)
    
    def remove_edges_from(self, edge_list):
        for edge in edge_list:
            self.remove_edge(edge[0],edge[1])