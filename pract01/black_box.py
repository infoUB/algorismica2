# uncompyle6 version 3.2.3
# Python bytecode 3.6 (3379)
# Decompiled from: Python 3.6.6 |Anaconda, Inc.| (default, Jun 28 2018, 17:14:51) 
# [GCC 7.2.0]
# Embedded file name: black_box.py
# Compiled at: 2018-10-21 17:28:36
# Size of source mod 2**32: 2069 bytes
import heapq as hq

def real_d2(G, origen, destino, penalty=20, infinity=float('inf')):
    unvisited = []
    visited = dict()
    distance = dict()
    parent = dict()
    lines = dict()
    for node in G.nodes():
        node_lines = set([G[node][neigh]['line'] for neigh in G[node].keys()])
        for line in node_lines:
            distance[(node, line)] = infinity
            visited[(node, line)] = False
            parent[(node, line)] = None

        lines[node] = node_lines

    for line in lines[origen]:
        distance[(origen, line)] = 0
        hq.heappush(unvisited, (0, origen, line))

    expanded = 0
    l_destino = -1
    while unvisited:
        d_current, current, l_current = hq.heappop(unvisited)
        if current == destino:
            l_destino = l_current
            expanded += 1
            break
        if not visited[(current, l_current)]:
            expanded += 1
            for node in G.neighbors(current):
                line = G[current][node]['line']
                if not visited[(node, line)]:
                    new_distance = d_current + G[current][node]['distance']
                    new_distance += 0 if line == l_current else penalty
                    if distance[(node, line)] > new_distance:
                        distance[(node, line)] = new_distance
                        parent[(node, line)] = (current, l_current)
                        hq.heappush(unvisited, (new_distance, node, line))

            visited[(current, l_current)] = True

    current = (
     destino, l_destino)
    path = []
    if l_destino > 0:
        if distance[current] < infinity:
            while current:
                path.append(current[0])
                current = parent[current]

            path.reverse()
    return {'path':path, 
     'expanded':expanded, 
     'distance':distance[(destino, l_destino)]}